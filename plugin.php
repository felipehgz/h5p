<?php

require_once __DIR__ . "/vendor/autoload.php";

$id = "xhfp";
$version = "3.0.0";
$ilias_min_version = "6.0";
$ilias_max_version = "7.999";
$responsible       = "studer + raimann ag - Team Core 1";
$responsible_mail  = "support-core1@studer-raimann.ch";